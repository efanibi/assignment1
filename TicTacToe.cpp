#include "TicTacToe.h"
TicTacToe::TicTacToe(){
    for(int i = 0; i < 3; i++)
        for(int j = 0; j < 3; j++)
             board[i][j] = ' ';
    char players[2]={'X','O'};
    int playernumber;
    playernumber=(rand()%2);
    currentplayer=players[playernumber];
}

//TicTacToe::TicTacToe(){
//}


void TicTacToe::print()
{
    for(int i = 0; i < 3; i++)
         for(int j = 0; j < 3; j++)
         {
             cout<<board[i][j];
             if(j==2)
             {
                   cout<<endl;
             }
         }

}

char TicTacToe::getCurrentPlayer()
{
    int ocounter=0;
    int xcounter=0;

    for(int i=0;i<3;i++)
    {
        for(int j=0;j<3;j++)
        {
            if(board[i][j]=='O')
                ocounter++;
            if(board[i][j]=='X')
                xcounter++;
        }
    }
    //Changes Player only if valid move has been made

    if(currentplayer=='X')
        currentplayer='O';
    else if(currentplayer=='O')
        currentplayer='X';

    return currentplayer;
}

void TicTacToe::makeMove(int row,int column)
{
//array starts at zero
    row=row-1;
    column=column-1;
    char placement=currentplayer;
    board[row][column]=placement;
}
bool TicTacToe::isValidMove(int row, int column)
{
    //array starts at zero
    row=row-1;
    column=column-1;
    if(board[row][column]!=' ')
    {
        return false;
    }
    else if(row>2 or column>2)
    {
        return false;
    }
    return true;
}
bool TicTacToe::isDone()
{
    int movesleft=9;
    bool doneyet=0;
    char checkwinner = getWinner();
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<3;j++)
     {
         if(board[i][j]!=' ')
             movesleft--;
     }

    }
    if(movesleft==0)
        doneyet=1;
    if(checkwinner!=' ')
        doneyet=1;
    return doneyet;
}


char TicTacToe::getWinner()
{
    //Horizontal
    char winner;
    int counter;
    for(int i=0;i<3;i++)
    {
        winner=' ';
        counter=0;

        for(int j=0;j<2;j++)
        {
            if(board[i][j]==board[i][j+1] and board[i][j]!=' ')
                counter++;
            if(counter==2)
            {
           winner=board[i][j];
           return winner;
            }
       }
     }
//Vertical
    for(int j=0;j<3;j++)
    {
        winner=' ';
        counter=0;

        for(int i=0;i<2;i++)
        {
            if(board[i][j]==board[i+1][j] and board[i][j]!=' ')
                counter++;
            if(counter==2)
            {
                winner=board[i][j];
                return winner;
            }
        }
    }
//Dianginal
    counter=0;
    for(int j=0,i=0;j<2;j++,i++)
    {

        if(board[i][j]==board[i+1][j+1] and  board[i][j]!=' ')
            counter++;
        if(counter==2)
        {
            winner=board[i][j];
            return winner;
        }
    }
    counter=0;
    for(int j=2,i=0;i<2;j--,i++)
    {

        if(board[i][j]==board[i+1][j-1] and  board[i][j]!=' ')
               counter++;
        if(counter==2)
        {
                winner=board[i][j];
                return winner;
        }
    }
return winner;
}
void TicTacToe::makeAutoMove()
{
    int row;
    int column;
    char placement=currentplayer;
    row= 5;
    column= 5;
    bool test=isValidMove(row,column);
    while(!test)
    {
      row= rand() %3+1;
      column= rand() %3+1;
      test=(isValidMove(row,column));
    }
    //sync rows and column values
    row=row-1;
    column=column-1;

   board[row][column]=placement;
}



