#include <iostream>
using namespace std;
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#ifndef TicTacToe_H
#define TicTacToe_H

/**
 * @brief The TicTacToe class Creates a simple TicTac Toe Game Object
 */

class TicTacToe{
public:
    /**
 * @brief TicTacToe Creates a Tic TacToe Board
 */
TicTacToe();

/**
 * @brief print Display current board, based on player or computer input
 */
void print();
/**
 * @brief getCurrentPlayer Switches to other player
 * @return the other player to the function call
 */
char getCurrentPlayer();
/**
 * @brief makeMove inputs move into Tic Tac Toe Board. First Player is selected at random
 * @param row the player is inputting to
 * @param column the player is inputting to
 */
void makeMove(int row,int column);
/**
 * @brief isValidMove Checks if spaces is avalible and is also within the range of play
 * @param row checked if row is avalible
 * @param column check if column is availble
 * @return true or false based on input
 */
bool isValidMove(int row,int column);
/**
 * @brief isDone check if game board is filled for ties, or if game has been won.
 * @return
 */
bool isDone();
/**
 * @brief getWinner Checks for diangle, Horizontal, and Vertical wins
 * @return
 */
char getWinner();
/**
 * @brief makeAutoMove Allows player to pick a random spot, since the board is so small. Just keeps trying until a free spot is found.
 */
void makeAutoMove();
private:
char currentplayer;
char board[3][3];


};



#endif // TicTacToe_H

