#include <iostream>
#include "TicTacToe.h"

using namespace std;

int main()
{
    cout << "Tic Tac Toe v0.1" << endl << endl;
    srand (time(0));
    //create a TicTacToe object
    TicTacToe theGame;

    while(!theGame.isDone())
  {

        //show current state
        theGame.print();

        //ask who the current player is
        char playerName = theGame.getCurrentPlayer();

        bool automover;
        cout <<endl<<playerName << "'s turn"<<endl;
        cout<<"Make Automove 1 yes or 0 no ";
        cin>>automover;
        if(automover)
              {

               theGame.makeAutoMove();
               continue;
              }

        //get their move
        cout << playerName << "'s turn. Enter row (1-3) and column (1-3): " << endl;
        int row, col;
        cin >> row >> col;

        //only make the move if it is valid...
        if(theGame.isValidMove(row, col)) {
              theGame.makeMove(row, col);
             //makeMove should change turn to next player
         }
        else {
              cout << "Invalid move." << endl;
             //will redo this player's turn next time through loop
             }



          }

     char winner = theGame.getWinner();
      theGame.print();

     cout << winner << " wins!"<< endl;
}
